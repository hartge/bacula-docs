%%
%%

\chapter{Client/File daemon Configuration}
\label{FiledConfChapter}
\index[general]{Configuration!Client/File daemon }
\index[general]{Client/File daemon Configuration }

The Client (or File Daemon) Configuration is one of the simpler ones to
specify. Generally, other than changing the Client name so that error messages
are easily identified, you will not need to modify the default Client
configuration file. 

For a general discussion of configuration file and resources including the
data types recognized by {\bf Bacula}, please see the 
\ilink{Configuration}{ConfigureChapter} chapter of this manual. The
following Client Resource definitions must be defined: 

\begin{itemize}
\item 
   \ilink{Client}{ClientResource} -- to define what Clients are  to
   be backed up.  
\item 
   \ilink{Director}{DirectorResource} -- to  define the Director's
   name and its access password.  
\item 
   \ilink{Messages}{MessagesChapter} -- to define where error  and
   information messages are to be sent. 
\end{itemize}

\section{The Client Resource}
\label{ClientResource}
\index[general]{Resource!Client }
\index[general]{Client Resource }

The Client Resource (or FileDaemon) resource defines the name of the Client
(as used by the Director) as well as the port on which the Client listens for
Director connections. 

\begin{description}

\item [Client (or FileDaemon)]
   \index[fd]{Client (or FileDaemon)}
   \index[fd]{Directive!Client (or FileDaemon)}
   Start of the Client records.  There must be one and only one Client resource
   in the  configuration file, since it defines the properties of the  current
   client program. 

\item [Name = \lt{}name\gt{}]
   \index[fd]{Name}
   \index[fd]{Directive!Name}
   The client name that must be used  by the Director when connecting. Generally,
   it is a good idea  to use a name related to the machine so that error messages
   can be easily identified if you have multiple Clients.  This directive is
   required.  

\item [Working Directory = \lt{}Directory\gt{}]
   \index[fd]{Working Directory}
   \index[fd]{Directive!Working Directory}
   This directive  is mandatory and specifies a directory in which the File
   daemon  may put its status files. This directory should be used only  by {\bf
   Bacula}, but may be shared by other Bacula daemons provided the daemon
   names on the {\bf Name} definition are unique for each daemon. This directive
   is required. 

   On Win32 systems, in some circumstances you may need to specify a drive
   letter in the specified working directory path.  Also, please be sure
   that this directory is writable by the SYSTEM user otherwise restores
   may fail (the bootstrap file that is transferred to the File daemon from
   the Director is temporarily put in this directory before being passed
   to the Storage daemon).

\item [Pid Directory = \lt{}Directory\gt{}]
   \index[fd]{Pid Directory}
   \index[fd]{Directive!Pid Directory}
   This directive  is mandatory and specifies a directory in which the Director 
   may put its process Id file files. The process Id file is used to  shutdown
   Bacula and to prevent multiple copies of  Bacula from running simultaneously. 
   This record is required. Standard shell expansion of the {\bf Directory}  is
   done when the configuration file is read so that values such  as {\bf \$HOME}
   will be properly expanded.  

   Typically on Linux systems, you will set this to:  {\bf /var/run}. If you are
   not installing Bacula in the  system directories, you can use the {\bf Working
   Directory} as  defined above. 

\item [Heartbeat Interval = \lt{}time-interval\gt{}]
   \index[fd]{Heartbeat Interval}
   \index[fd]{Directive!Heartbeat Interval}
   \index[general]{Heartbeat Interval}
   \index[general]{Broken pipe}
   \index[general]{slow}
   \index[general]{Backups!slow}
   This record defines an interval of time in seconds.  For each heartbeat that the
   File daemon receives from the Storage daemon, it will forward it to the
   Director.  In addition, if no heartbeat has been received from the
   Storage daemon and thus forwarded the File daemon will send a heartbeat
   signal to the Director and to the Storage daemon to keep the channels
   active.  The default interval is zero which disables the heartbeat.
   This feature is particularly useful if you have a router such as 3Com
   that does not follow Internet standards and times out a valid    
   connection after a short duration despite the fact that keepalive is
   set. This usually results in a broken pipe error message.

   If you continue getting broken pipe error messages despite using the
   Heartbeat Interval, and you are using Windows, you should consider
   upgrading your ethernet driver.  This is a known problem with NVidia
   NForce 3 drivers (4.4.2 17/05/2004), or try the following workaround
   suggested by Thomas Simmons for Win32 machines:

   Browse to:
   Start \gt{} Control Panel \gt{} Network Connections

   Right click the connection for the nvidia adapter and select properties. 
   Under the General tab, click "Configure...". Under the Advanced tab set 
   "Checksum Offload" to disabled and click OK to save the change.  
   
   Lack of communications, or communications that get interrupted can
   also be caused by Linux firewalls where you have a rule that throttles
   connections or traffic.


\item [Maximum Concurrent Jobs = \lt{}number\gt{}]
   \index[fd]{Maximum Concurrent Jobs}
   \index[fd]{Directive!Maximum Concurrent Jobs}
   where \lt{}number\gt{} is the maximum number of Jobs that should run
   concurrently.  The default is set to 2, but you may set it to a larger
   number.  Each contact from the Director (e.g.  status request, job start
   request) is considered as a Job, so if you want to be able to do a {\bf
   status} request in the console at the same time as a Job is running, you
   will need to set this value greater than 1.

\item [FDAddresses = \lt{}IP-address-specification\gt{}]
   \index[fd]{FDAddresses}
   \index[fd]{Directive!FDAddresses}
   Specify the ports and addresses on which the File daemon listens for
   Director connections.  Probably the simplest way to explain is to show
   an example:

\footnotesize
\begin{verbatim}
 FDAddresses  = { 
    ip = { addr = 1.2.3.4; port = 1205; }
    ipv4 = {
        addr = 1.2.3.4; port = http; }
    ipv6 = {
        addr = 1.2.3.4;
        port = 1205;
    }
    ip = {
        addr = 1.2.3.4
        port = 1205
    }
    ip = { addr = 1.2.3.4 }
    ip = {
        addr = 201:220:222::2
    }
    ip = {
        addr = bluedot.thun.net
    }
 }
\end{verbatim}
\normalsize

where ip, ip4, ip6, addr, and port are all keywords. Note, that  the address
can be specified as either a dotted quadruple, or  IPv6 colon notation, or as
a symbolic name (only in the ip specification).  Also, port can be specified
as a number or as the mnemonic value from  the /etc/services file.  If a port
is not specified, the default will be used. If an ip  section is specified,
the resolution can be made either by IPv4 or  IPv6. If ip4 is specified, then
only IPv4 resolutions will be permitted,  and likewise with ip6.  

\item [FDPort = \lt{}port-number\gt{}]
   \index[fd]{FDPort}
   \index[fd]{Directive!FDPort}
   This specifies the port number  on which the Client listens for Director
   connections. It must agree  with the FDPort specified in the Client resource
   of the Director's  configuration file. The default is 9102. 

\item [FDAddress = \lt{}IP-Address\gt{}]
   \index[fd]{FDAddress}
   \index[fd]{Directive!FDAddress}
   This record is optional,  and if it is specified, it will cause the File
   daemon server (for  Director connections) to bind to the specified {\bf
   IP-Address},  which is either a domain name or an IP address specified as a 
   dotted quadruple. If this record is not specified, the File daemon  will bind
   to any available address (the default). 

\item [FDSourceAddress = \lt{}IP-Address\gt{}]
   \index[fd]{FDSourceAddress}
   \index[fd]{Directive!FDSourceAddress}
   This record is optional,  and if it is specified, it will cause the File
   daemon server (for  Storage connections) to bind to the specified {\bf
   IP-Address},  which is either a domain name or an IP address specified as a
   dotted quadruple. If this record is not specified, the kernel will choose 
   the best address according to the routing table (the default).

\item [SDConnectTimeout = \lt{}time-interval\gt{}]
   \index[fd]{SDConnectTimeout}
   \index[fd]{Directive!SDConnectTimeout}
   This  record defines an interval of time that  the File daemon will try to
   connect to the  Storage daemon. The default is 30 minutes. If no connection 
   is made in the specified time interval, the File daemon  cancels the Job. 

\item [Maximum Network Buffer Size = \lt{}bytes\gt{}]  
   \index[fd]{Maximum Network Buffer Size}
   \index[fd]{Directive!Maximum Network Buffer Size}
   where \lt{}bytes\gt{} specifies the initial network buffer  size to use with
   the File daemon. This size will be adjusted down  if it is too large until it
   is accepted by the OS. Please use  care in setting this value since if it is
   too large, it will  be trimmed by 512 bytes until the OS is happy, which may
   require  a large number of system calls. The default value is 65,536 bytes. 

   Note, on certain Windows machines, there are reports that the
   transfer rates are very slow and this seems to be related to
   the default 65,536 size. On systems where the transfer rates
   seem abnormally slow compared to other systems, you might try
   setting the Maximum Network Buffer Size to 32,768 in both the
   File daemon and in the Storage daemon.

\item [Maximum Bandwidth Per Job = \lt{}speed\gt{}]
\index[fd]{Maximum Bandwidth Per Job}
\index[fd]{Directive!Maximum Bandwidth Per Job}

The speed parameter specifies the maximum allowed bandwidth that a job may
use. The speed parameter should be specified in k/s, kb/s, m/s or mb/s.

\item [Heartbeat Interval = \lt{}time-interval\gt{}]
   \index[console]{Heartbeat Interval}
   \index[console]{Directive!Heartbeat}
   This directive is optional and if specified will cause the File daemon to
   set a keepalive interval (heartbeat) in seconds on each of the sockets
   to communicate with the Storage daemon.  It is implemented only on systems
   (Linux, ...) that provide the {\bf setsockopt} TCP\_KEEPIDLE function.
   The default value is zero, which means no change is made to the socket.


\item [PKI Encryption]
  See the \ilink{Data Encryption}{DataEncryption} chapter of this manual. 

\item [PKI Signatures]
  See the \ilink{Data Encryption}{DataEncryption} chapter of this manual. 

\item [PKI Keypair]
  See the \ilink{Data Encryption}{DataEncryption} chapter of this manual. 

\item [PKI Master Key]
  See the \ilink{Data Encryption}{DataEncryption} chapter of this manual. 

\end{description}

The following is an example of a valid Client resource definition: 

\footnotesize
\begin{verbatim}
Client {                              # this is me
  Name = rufus-fd
  WorkingDirectory = $HOME/bacula/bin/working
  Pid Directory = $HOME/bacula/bin/working
}
\end{verbatim}
\normalsize

\section{The Director Resource}
\label{DirectorResource}
\index[general]{Director Resource }
\index[general]{Resource!Director }

The Director resource defines the name and password of the Directors that are
permitted to contact this Client. 

\begin{description}

\item [Director]
   \index[fd]{Director}
   \index[fd]{Directive!Director}
   Start of the Director records. There may be any  number of Director resources
   in the Client configuration file. Each  one specifies a Director that is
   allowed to connect to this  Client. 

\item [Name = \lt{}name\gt{}]
   \index[fd]{Name}
   \index[fd]{Directive!Name}
   The name of the Director  that may contact this Client. This name must be the 
   same as the name specified on the Director resource  in the Director's
   configuration file. Note, the case (upper/lower) of the characters in
   the name are significant (i.e. S is not the same as s). This directive
   is required. 

\item [Password = \lt{}password\gt{}]
   \index[fd]{Password}
   \index[fd]{Directive!Password}
   Specifies the password that must be  supplied for a Director to be authorized.
This password  must be the same as the password specified in the  Client
resource in the Director's configuration file.  This directive is required. 

\item [Maximum Bandwidth Per Job = \lt{}speed\gt{}]
\index[fd]{Maximum Bandwidth Per Job}
\index[fd]{Directive!Maximum Bandwidth Per Job}

The speed parameter specifies the maximum allowed bandwidth that a job may use
when started from this Director. The speed parameter should be specified in
k/s, Kb/s, m/s or Mb/s.

\item [Monitor = \lt{}yes\vb{}no\gt{}]
   \index[fd]{Monitor}
   \index[fd]{Directive!Monitor}
   If Monitor is set to {\bf no} (default),  this director will have full access
   to this Client. If Monitor is set to  {\bf yes}, this director will only be
   able to fetch the current status  of this Client.

   Please note that if this director is being used by a Monitor, we highly 
   recommend to set this directive to {\bf yes} to avoid serious security 
   problems. 
\end{description}

Thus multiple Directors may be authorized to use this Client's services. Each
Director will have a different name, and normally a different password as
well. 

The following is an example of a valid Director resource definition: 

\footnotesize
\begin{verbatim}
#
# List Directors who are permitted to contact the File daemon
#
Director {
  Name = HeadMan
  Password = very_good                # password HeadMan must supply
}
Director {
  Name = Worker
  Password = not_as_good
  Monitor = Yes
}
\end{verbatim}
\normalsize

\section{The Message Resource}
\label{MessagesResource3}
\index[general]{Message Resource}
\index[general]{Resource!Message }

Please see the 
\ilink{Messages Resource}{MessagesChapter} Chapter of this
manual for the details of the Messages Resource. 

There must be at least one Message resource in the Client configuration file. 

\section{Example Client Configuration File}
\label{SampleClientConfiguration}
\index[general]{Example Client Configuration File }
\index[general]{File!Example Client Configuration }

An example File Daemon configuration file might be the following: 

\footnotesize
\begin{verbatim}
#
# Default  Bacula File Daemon Configuration file
#
#  For Bacula release 1.35.2 (16 August 2004) -- gentoo 1.4.16
#
# There is not much to change here except perhaps to
#   set the Director's name and File daemon's name
#   to something more appropriate for your site.
#
#
# List Directors who are permitted to contact this File daemon
#
Director {
  Name = rufus-dir
  Password = "/LqPRkX++saVyQE7w7mmiFg/qxYc1kufww6FEyY/47jU"
}
#
# Restricted Director, used by tray-monitor to get the
#   status of the file daemon
#
Director {
  Name = rufus-mon
  Password = "FYpq4yyI1y562EMS35bA0J0QC0M2L3t5cZObxT3XQxgxppTn"
  Monitor = yes
}
#
# "Global" File daemon configuration specifications
#
FileDaemon {                          # this is me
  Name = rufus-fd
  WorkingDirectory = $HOME/bacula/bin/working
  Pid Directory = $HOME/bacula/bin/working
}
# Send all messages except skipped files back to Director
Messages {
  Name = Standard
  director = rufus-dir = all, !skipped
}
\end{verbatim}
\normalsize
