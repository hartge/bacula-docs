IMAGES=../../../../images
DOC=main
MAINDOC=Main_Reference.html
#
# Web documentation generation
HTML_TITLE="Main Reference"
HTML_DEPTH=3
HTML_TITLES=6
HTML_VERSION=4.0
LATEX_TO_HTML=latex2html -split ${HTML_DEPTH} -local_icons -t ${HTML_TITLE} -long_titles ${HTML_TITLES} -toc_stars -contents_in_nav -init_file ./latex2html-init.pl -no_antialias -no_antialias_text -white -notransparent ${DOC} -html_version ${HTML_VERSION} >tex.out 2>&1

.SUFFIXES:     .tex .html
.PHONY:
.DONTCARE:

first_rule: all

all: web clean

pngimages:
	@echo "PNG images"
	@echo -n "Generating PNG images..."
	@(cd ${IMAGES}/svg ; make png)
	@echo "Done."
	@echo -n "Linking png images..."
	@(for F in $(IMAGES)/*.png ;\
	do \
	ln -sf $$F ./`basename $$F`; \
	done)
	@(for F in $(IMAGES)/png/*.png ;\
	do \
	ln -sf $$F ./`basename $$F`; \
	done)
	@echo "Done."
	@echo "PNG done."

clean:
	@echo -n "Cleaning..."
	@find . -type l -iname "*.png" -exec rm -f {} \;
	@echo "Done."

web: pngimages
	@echo "Making web"
	@rm -f	next.png  prev.png  up.png
	@rm -f ${DOC}/xp-*.png
	@rm -f	${DOC}/next.png  ${DOC}/prev.png  ${DOC}/up.png
	@${LATEX_TO_HTML}
	@cp -f ${DOC}/${MAINDOC} ${DOC}/index.html
	@echo "Done making web"
