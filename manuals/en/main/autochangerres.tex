%%
\chapter{Autochanger Resource}
\index[sd]{Autochanger Resource}
\index[sd]{Resource!Autochanger}

The Autochanger resource supports single or multiple drive
autochangers by grouping one or more Device resources     
into one unit called an autochanger in Bacula (often referred to
as a "tape library" by autochanger manufacturers).

If you have an Autochanger, and you want it to function correctly,
you {\bf must} have an Autochanger resource in your Storage
conf file, and your Director's Storage directives that want to
use an Autochanger {\bf must} refer to the Autochanger resource name.
In previous versions of Bacula, the Director's Storage directives
referred directly to Device resources that were autochangers.     
In version 1.38.0 and later, referring directly to Device resources
will not work for Autochangers.

\begin{description}
\item [Name = \lt{}Autochanger-Name\gt{}]
   \index[sd]{Name}
   Specifies the Name of the Autochanger.  This name is used in the
   Director's Storage definition to refer to the autochanger.  This
   directive is required.

\item [Device = \lt{}Device-name1, device-name2, ...\gt{}]
   Specifies the names of the Device resource or resources that correspond
   to the autochanger drive.  If you have a multiple drive autochanger, you
   must specify multiple Device names, each one referring to a separate
   Device resource that contains a Drive Index specification that
   corresponds to the drive number base zero.  You may specify multiple
   device names on a single line separated by commas, and/or you may
   specify multiple Device directives.  This directive is required.

\item [Changer Device = \lt{}device-name\gt{}]
   \index[sd]{Changer Device  }
   In addition to the Archive Device name, you may specify a  {\bf Changer
Device} name. This is because most autochangers are  controlled through a
different device than is used for reading and  writing the cartridges. For
example, on Linux, one normally uses the generic SCSI interface for
controlling the autochanger, but the standard SCSI interface for reading and
writing the  tapes. On Linux, for the {\bf Archive Device = /dev/nst0},  you
would typically have {\bf Changer Device = /dev/sg0}.  Note, some of the more
advanced autochangers will locate the changer device on {\bf /dev/sg1}. Such
devices typically have  several drives and a large number of tapes.  

On FreeBSD systems, the changer device will typically be on {\bf /dev/pass0}
through {\bf /dev/passn}.  

On Solaris, the changer device will typically be some file under {\bf
/dev/rdsk}.  

Please ensure that your Storage daemon has permission to access this
device.

\item [Changer Command = \lt{}command\gt{}]
   \index[sd]{Changer Command  }
   This record is used to specify the external program to call  and what
arguments to pass to it. The command is assumed to be  a standard program or
shell script that can be executed by  the operating system. This command is
invoked each time that Bacula wishes to manipulate the autochanger.  The
following substitutions are made in the {\bf command}  before it is sent to
the operating system for execution:  

\footnotesize
\begin{verbatim}
      %% = %
      %a = archive device name
      %c = changer device name
      %d = changer drive index base 0
      %f = Client's name
      %j = Job name
      %o = command  (loaded, load, or unload)
      %s = Slot base 0
      %S = Slot base 1
      %v = Volume name
\end{verbatim}
\normalsize

An actual example for using {\bf mtx} with the  {\bf mtx-changer} script (part
of the Bacula distribution) is:  

\footnotesize
\begin{verbatim}
Changer Command = "/etc/bacula/mtx-changer %c %o %S %a %d"
\end{verbatim}
\normalsize

Where you will need to adapt the {\bf /etc/bacula} to be  the actual path on
your system where the mtx-changer script  resides.  Details of the three
commands currently used by Bacula  (loaded, load, unload) as well as the
output expected by  Bacula are give in the {\bf Bacula Autochanger Interface} 
section below.  

\end{description}

The following is an example of a valid Autochanger resource definition: 

\footnotesize
\begin{verbatim}
Autochanger {
  Name = "DDS-4-changer"
  Device = DDS-4-1, DDS-4-2, DDS-4-3
  Changer Device = /dev/sg0
  Changer Command = "/etc/bacula/mtx-changer %c %o %S %a %d"
}
Device {
  Name = "DDS-4-1"
  Drive Index = 0
  Autochanger = yes
  ...
}
Device {
  Name = "DDS-4-2"
  Drive Index = 1
  Autochanger = yes
  ...
Device {
  Name = "DDS-4-3"
  Drive Index = 2
  Autochanger = yes
  Autoselect = no
  ...
}
\end{verbatim}
\normalsize

Please note that it is important to include the {\bf Autochanger = yes} directive
in each Device definition that belongs to an Autochanger.  A device definition
should not belong to more than one Autochanger resource.  Also, your Device
directive in the Storage resource of the Director's conf file should have
the Autochanger's resource name rather than a name of one of the Devices.

If you have a drive that physically belongs to an Autochanger but you don't want
to have it automatically used when Bacula references the Autochanger for backups,
for example, you want to reserve it for restores, you can add the directive:

\footnotesize
\begin{verbatim}
Autoselect = no
\end{verbatim}
\normalsize

to the Device resource for that drive. In that case, Bacula will not automatically
select that drive when accessing the Autochanger. You can, still use the drive
by referencing it by the Device name directly rather than the Autochanger name. An example
of such a definition is shown above for the Device DDS-4-3, which will not be
selected when the name DDS-4-changer is used in a Storage definition, but will
be used if DDS-4-3 is used.
