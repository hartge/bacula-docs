\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Variable Expansion}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}General Functionality}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Bacula Variables}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Full Syntax}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Semantics}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Examples}{3}{section.1.5}
\contentsline {chapter}{\numberline {2}Using Stunnel to Encrypt Communications}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Communications Ports Used}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Encryption}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}A Picture}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}Certificates}{6}{section.2.4}
\contentsline {section}{\numberline {2.5}Securing the Data Channel}{6}{section.2.5}
\contentsline {section}{\numberline {2.6}Data Channel Configuration}{7}{section.2.6}
\contentsline {section}{\numberline {2.7}Stunnel Configuration for the Data Channel}{7}{section.2.7}
\contentsline {section}{\numberline {2.8}Starting and Testing the Data Encryption}{8}{section.2.8}
\contentsline {section}{\numberline {2.9}Encrypting the Control Channel}{8}{section.2.9}
\contentsline {section}{\numberline {2.10}Control Channel Configuration}{9}{section.2.10}
\contentsline {section}{\numberline {2.11}Stunnel Configuration for the Control Channel}{9}{section.2.11}
\contentsline {section}{\numberline {2.12}Starting and Testing the Control Channel}{10}{section.2.12}
\contentsline {section}{\numberline {2.13}Using stunnel to Encrypt to a Second Client}{10}{section.2.13}
\contentsline {section}{\numberline {2.14}Creating a Self-signed Certificate}{11}{section.2.14}
\contentsline {section}{\numberline {2.15}Getting a CA Signed Certificate}{12}{section.2.15}
\contentsline {section}{\numberline {2.16}Using ssh to Secure the Communications}{12}{section.2.16}
\contentsline {chapter}{\numberline {3}Bacula Projects}{13}{chapter.3}
\contentsline {chapter}{\numberline {4}Bacula Copyright, Trademark, and Licenses}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}CC-BY-SA}{15}{section.4.1}
\contentsline {section}{\numberline {4.2}GPL}{15}{section.4.2}
\contentsline {section}{\numberline {4.3}LGPL}{15}{section.4.3}
\contentsline {section}{\numberline {4.4}Public Domain}{15}{section.4.4}
\contentsline {section}{\numberline {4.5}Trademark}{16}{section.4.5}
\contentsline {section}{\numberline {4.6}Fiduciary License Agreement}{16}{section.4.6}
\contentsline {section}{\numberline {4.7}Disclaimer}{16}{section.4.7}
\contentsline {section}{\numberline {4.8}Authors}{16}{section.4.8}
\contentsline {section}{\numberline {4.9}Table of Contents}{29}{section.4.9}
\contentsline {section}{\numberline {4.10}GNU LESSER GENERAL PUBLIC LICENSE}{29}{section.4.10}
\contentsline {section}{\numberline {4.11}Preamble}{29}{section.4.11}
\contentsline {section}{\numberline {4.12}TERMS AND CONDITIONS}{30}{section.4.12}
\contentsline {section}{\numberline {4.13}How to Apply These Terms to Your New Libraries}{35}{section.4.13}
