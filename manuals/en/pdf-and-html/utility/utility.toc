\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Volume Utility Tools}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Specifying the Configuration File}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Specifying a Device Name For a Tape}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}Specifying a Device Name For a File}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Specifying Volumes}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}bls}{6}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Listing Jobs}{7}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}Listing Blocks}{7}{subsection.1.5.2}
\contentsline {section}{\numberline {1.6}bextract}{8}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Extracting with Include or Exclude Lists}{9}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}Extracting With a Bootstrap File}{10}{subsection.1.6.2}
\contentsline {subsection}{\numberline {1.6.3}Extracting From Multiple Volumes}{10}{subsection.1.6.3}
\contentsline {section}{\numberline {1.7}bscan}{10}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}Using bscan to Compare a Volume to an existing Catalog}{12}{subsection.1.7.1}
\contentsline {subsection}{\numberline {1.7.2}Using bscan to Recreate a Catalog from a Volume}{12}{subsection.1.7.2}
\contentsline {subsection}{\numberline {1.7.3}Using bscan to Correct the Volume File Count}{14}{subsection.1.7.3}
\contentsline {subsection}{\numberline {1.7.4}After bscan}{14}{subsection.1.7.4}
\contentsline {section}{\numberline {1.8}bcopy}{14}{section.1.8}
\contentsline {subsection}{\numberline {1.8.1}bcopy Command Options}{14}{subsection.1.8.1}
\contentsline {section}{\numberline {1.9}btape}{15}{section.1.9}
\contentsline {subsection}{\numberline {1.9.1}Using btape to Verify your Tape Drive}{15}{subsection.1.9.1}
\contentsline {subsection}{\numberline {1.9.2}btape Commands}{15}{subsection.1.9.2}
\contentsline {section}{\numberline {1.10}Other Programs}{17}{section.1.10}
\contentsline {section}{\numberline {1.11}bsmtp}{17}{section.1.11}
\contentsline {section}{\numberline {1.12}dbcheck}{18}{section.1.12}
\contentsline {section}{\numberline {1.13}bregex}{21}{section.1.13}
\contentsline {section}{\numberline {1.14}bwild}{21}{section.1.14}
\contentsline {section}{\numberline {1.15}testfind}{21}{section.1.15}
\contentsline {section}{\numberline {1.16}bimagemgr}{23}{section.1.16}
\contentsline {subsection}{\numberline {1.16.1}bimagemgr installation}{23}{subsection.1.16.1}
\contentsline {subsection}{\numberline {1.16.2}bimagemgr usage}{24}{subsection.1.16.2}
\contentsline {chapter}{\numberline {2}Bacula RPM Packaging FAQ}{27}{chapter.2}
\contentsline {section}{\numberline {2.1}Answers}{27}{section.2.1}
\contentsline {section}{\numberline {2.2}Build Options}{30}{section.2.2}
\contentsline {section}{\numberline {2.3}RPM Install Problems}{31}{section.2.3}
\contentsline {chapter}{\numberline {3}Bacula Copyright, Trademark, and Licenses}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}CC-BY-SA}{33}{section.3.1}
\contentsline {section}{\numberline {3.2}GPL}{33}{section.3.2}
\contentsline {section}{\numberline {3.3}LGPL}{33}{section.3.3}
\contentsline {section}{\numberline {3.4}Public Domain}{33}{section.3.4}
\contentsline {section}{\numberline {3.5}Trademark}{34}{section.3.5}
\contentsline {section}{\numberline {3.6}Fiduciary License Agreement}{34}{section.3.6}
\contentsline {section}{\numberline {3.7}Disclaimer}{34}{section.3.7}
\contentsline {section}{\numberline {3.8}Authors}{34}{section.3.8}
