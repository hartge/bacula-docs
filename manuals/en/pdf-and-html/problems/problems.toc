\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Bacula Frequently Asked Questions}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}What is Bacula?}{7}{section.1.1}
\contentsline {section}{\numberline {1.2}Does Bacula support Windows?}{7}{section.1.2}
\contentsline {section}{\numberline {1.3}What language is Bacula written in?}{7}{section.1.3}
\contentsline {section}{\numberline {1.4}On what machines does Bacula run?}{7}{section.1.4}
\contentsline {section}{\numberline {1.5}Is Bacula Stable?}{8}{section.1.5}
\contentsline {section}{\numberline {1.6}I'm Getting Authorization Errors. What is Going On? }{8}{section.1.6}
\contentsline {section}{\numberline {1.7}Bacula Runs Fine but Cannot Access a Client on a Different Machine. Why? }{9}{section.1.7}
\contentsline {section}{\numberline {1.8}My Catalog is Full of Test Runs, How Can I Start Over?}{10}{section.1.8}
\contentsline {section}{\numberline {1.9}I Run a Restore Job and Bacula Hangs. What do I do?}{10}{section.1.9}
\contentsline {section}{\numberline {1.10}I Cannot Get My Windows Client to Start Automatically? }{10}{section.1.10}
\contentsline {section}{\numberline {1.11}My Windows Client Immediately Dies When I Start It}{10}{section.1.11}
\contentsline {section}{\numberline {1.12}My backups are not working on my Windows Client. What should I do?}{11}{section.1.12}
\contentsline {section}{\numberline {1.13}All my Jobs are scheduled for the same time. Will this cause problems?}{11}{section.1.13}
\contentsline {section}{\numberline {1.14}Can Bacula Backup My System To Files instead of Tape?}{11}{section.1.14}
\contentsline {section}{\numberline {1.15}Can I use a dummy device to test the backup?}{12}{section.1.15}
\contentsline {section}{\numberline {1.16}Can Bacula Backup and Restore Files Bigger than 2 Gigabytes?}{12}{section.1.16}
\contentsline {section}{\numberline {1.17}I want to stop a job.}{12}{section.1.17}
\contentsline {section}{\numberline {1.18}Why have You Trademarked the Name Bacula?}{12}{section.1.18}
\contentsline {section}{\numberline {1.19}Why is the Online Document for Version 1.39 but the Released Version is 1.38?}{12}{section.1.19}
\contentsline {section}{\numberline {1.20}Does Bacula really save and restore all files?}{13}{section.1.20}
\contentsline {section}{\numberline {1.21}I want an Incremental but Bacula runs it as a Full backup. Why?}{13}{section.1.21}
\contentsline {section}{\numberline {1.22}Do you really handle unlimited path lengths?}{13}{section.1.22}
\contentsline {section}{\numberline {1.23}What Is the Really Unique Feature of Bacula?}{13}{section.1.23}
\contentsline {section}{\numberline {1.24}How can I force one job to run after another?}{14}{section.1.24}
\contentsline {section}{\numberline {1.25}I Am Not Getting Email Notification, What Can I Do? }{14}{section.1.25}
\contentsline {section}{\numberline {1.26}My retention periods don't work}{14}{section.1.26}
\contentsline {section}{\numberline {1.27}Why aren't my files compressed?}{14}{section.1.27}
\contentsline {section}{\numberline {1.28}Incremental backups are not working}{15}{section.1.28}
\contentsline {section}{\numberline {1.29}I am waiting forever for a backup of an offsite machine}{15}{section.1.29}
\contentsline {section}{\numberline {1.30}SSH hangs forever after starting Bacula}{15}{section.1.30}
\contentsline {section}{\numberline {1.31}I'm confused by retention periods}{16}{section.1.31}
\contentsline {section}{\numberline {1.32}MaxVolumeSize is ignored}{16}{section.1.32}
\contentsline {section}{\numberline {1.33}I get a Connection refused when connecting to my Client}{16}{section.1.33}
\contentsline {section}{\numberline {1.34}Long running jobs die with Pipe Error}{16}{section.1.34}
\contentsline {section}{\numberline {1.35}How do I tell the Job which Volume to use?}{17}{section.1.35}
\contentsline {section}{\numberline {1.36}Password generation}{17}{section.1.36}
\contentsline {chapter}{\numberline {2}Tips and Suggestions}{19}{chapter.2}
\contentsline {section}{\numberline {2.1}Upgrading Bacula Versions}{19}{section.2.1}
\contentsline {section}{\numberline {2.2}Getting Notified of Job Completion}{19}{section.2.2}
\contentsline {section}{\numberline {2.3}Getting Email Notification to Work}{20}{section.2.3}
\contentsline {section}{\numberline {2.4}Getting Notified that Bacula is Running}{21}{section.2.4}
\contentsline {section}{\numberline {2.5}Maintaining a Valid Bootstrap File}{22}{section.2.5}
\contentsline {section}{\numberline {2.6}Rejected Volumes After a Crash}{23}{section.2.6}
\contentsline {section}{\numberline {2.7}Security Considerations}{25}{section.2.7}
\contentsline {section}{\numberline {2.8}Creating Holiday Schedules}{26}{section.2.8}
\contentsline {section}{\numberline {2.9}Automatic Labeling Using Your Autochanger}{26}{section.2.9}
\contentsline {section}{\numberline {2.10}Backing Up Portables Using DHCP}{27}{section.2.10}
\contentsline {section}{\numberline {2.11}Going on Vacation}{27}{section.2.11}
\contentsline {section}{\numberline {2.12}Exclude Files on Windows Regardless of Case}{28}{section.2.12}
\contentsline {section}{\numberline {2.13}Executing Scripts on a Remote Machine}{28}{section.2.13}
\contentsline {section}{\numberline {2.14}Recycling All Your Volumes}{29}{section.2.14}
\contentsline {section}{\numberline {2.15}Backing up ACLs on ext3 or XFS filesystems}{29}{section.2.15}
\contentsline {section}{\numberline {2.16}Total Automation of Bacula Tape Handling}{29}{section.2.16}
\contentsline {section}{\numberline {2.17}Running Concurrent Jobs}{30}{section.2.17}
\contentsline {chapter}{\numberline {3}Testing Your Tape Drive With Bacula}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}Get Your Tape Drive Working}{33}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Problems When no Tape in Drive}{34}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Specifying the Configuration File}{35}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Specifying a Device Name For a Tape}{35}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Specifying a Device Name For a File}{35}{subsection.3.1.4}
\contentsline {section}{\numberline {3.2}btape}{35}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Using btape to Verify your Tape Drive}{36}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Testing tape drive speed}{37}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Linux SCSI Tricks}{38}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Tips for Resolving Problems}{39}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Bacula Saves But Cannot Restore Files}{39}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Bacula Cannot Open the Device}{40}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Incorrect File Number}{41}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Incorrect Number of Blocks or Positioning Errors}{41}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Ensuring that the Tape Modes Are Properly Set -- {\bf Linux Only}}{42}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Tape Hardware Compression and Blocking Size}{43}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Tape Modes on FreeBSD}{44}{subsection.3.3.7}
\contentsline {subsection}{\numberline {3.3.8}Finding your Tape Drives and Autochangers on FreeBSD}{45}{subsection.3.3.8}
\contentsline {subsection}{\numberline {3.3.9}Using the OnStream driver on Linux Systems}{46}{subsection.3.3.9}
\contentsline {section}{\numberline {3.4}Hardware Compression on EXB-8900}{46}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Using btape to Simulate Filling a Tape}{46}{subsection.3.4.1}
\contentsline {section}{\numberline {3.5}Recovering Files Written With Fixed Block Sizes}{47}{section.3.5}
\contentsline {section}{\numberline {3.6}Tape Blocking Modes}{47}{section.3.6}
\contentsline {section}{\numberline {3.7}Details of Tape Modes}{48}{section.3.7}
\contentsline {section}{\numberline {3.8}Tape Performance Problems}{48}{section.3.8}
\contentsline {section}{\numberline {3.9}Autochanger Errors}{49}{section.3.9}
\contentsline {section}{\numberline {3.10}Syslog Errors}{49}{section.3.10}
\contentsline {chapter}{\numberline {4}Dealing with Firewalls}{51}{chapter.4}
\contentsline {section}{\numberline {4.1}Technical Details}{51}{section.4.1}
\contentsline {section}{\numberline {4.2}A Concrete Example}{51}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}The Bacula Configuration Files for the Above}{53}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}How Does It Work?}{54}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Important Note}{55}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Firewall Problems}{55}{subsection.4.2.4}
\contentsline {chapter}{\numberline {5}What To Do When Bacula Crashes (Kaboom)}{57}{chapter.5}
\contentsline {section}{\numberline {5.1}Traceback}{57}{section.5.1}
\contentsline {section}{\numberline {5.2}Testing The Traceback}{58}{section.5.2}
\contentsline {section}{\numberline {5.3}Getting A Traceback On Other Systems}{58}{section.5.3}
\contentsline {section}{\numberline {5.4}Manually Running Bacula Under The Debugger}{59}{section.5.4}
\contentsline {section}{\numberline {5.5}Getting Debug Output from Bacula}{59}{section.5.5}
\contentsline {chapter}{\numberline {6}Bacula Copyright, Trademark, and Licenses}{61}{chapter.6}
\contentsline {section}{\numberline {6.1}CC-BY-SA}{61}{section.6.1}
\contentsline {section}{\numberline {6.2}GPL}{61}{section.6.2}
\contentsline {section}{\numberline {6.3}LGPL}{61}{section.6.3}
\contentsline {section}{\numberline {6.4}Public Domain}{61}{section.6.4}
\contentsline {section}{\numberline {6.5}Trademark}{62}{section.6.5}
\contentsline {section}{\numberline {6.6}Fiduciary License Agreement}{62}{section.6.6}
\contentsline {section}{\numberline {6.7}Disclaimer}{62}{section.6.7}
\contentsline {section}{\numberline {6.8}Authors}{62}{section.6.8}
