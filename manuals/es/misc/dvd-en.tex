%%
%%

\chapter{DVD Volumes}
\label{_DVDChapterStart}
\index[general]{DVD Volumes}
\index[general]{Writing DVDs}
\index[general]{DVD Writing}
\index[general]{Volumes!DVD}

Bacula allows you to specify that you want to write to DVD. However,
this feature is implemented only in version 1.37 or later.
You may in fact write to DVD+RW, DVD+R, DVD-R, or DVD-RW 
media. The actual process used by Bacula is to first write
the image to a spool directory, then when the Volume reaches
a certain size or,  at your option, at the end of a Job, Bacula
will transfer the image from the spool directory to the
DVD.  The actual work of transferring the image is done
by a script {\bf dvd-handler}, and the heart of that
script is a program called {\bf growisofs} which allows
creating or adding to a DVD ISO filesystem.

You must have {\bf dvd+rw-tools} loaded on your system for DVD writing to
work.  Please note that the original {\bf dvd+rw-tools} package does {\bf
NOT} work with Bacula.  You must apply a patch which can be found in the
{\bf patches} directory of Bacula sources with the name
{\bf dvd+rw-tools-5.21.4.10.8.bacula.patch} for version 5.21 of the tools,
or patch {bf dvd+rw-tools-6.1.bacula.patch} if you have version 6.1       
on your system. Unfortunately, this requires you to build the dvd\_rw-tools
from source.

Note, some Linux distros such as Debian dvd+rw-tools-7.0-4 package already
have the patch applied, so please check.

The fact that Bacula cannot use the OS to write directly
to the DVD makes the whole process a bit more error prone than
writing to a disk or a tape, but nevertheless, it does work if you
use some care to set it up properly. However, at the current time
(version 1.39.30 -- 12 December 2006) we still consider this code to be
BETA quality.  As a consequence, please do careful testing before relying
on DVD backups in production.

The remainder of this chapter explains the various directives that you can
use to control the DVD writing.

\label{DVDdirectives}
\section{DVD Specific SD Directives} 
\index[general]{Directives!DVD}
\index[general]{DVD Specific SD Directives }

The following directives are added to the Storage daemon's
Device resource.

\begin{description}

\item [Requires Mount = {\it Yes|No}]
   \index[general]{Requires Mount  }
   You must set this directive to {\bf yes} for DVD-writers,  and to {\bf no} for
   all other devices (tapes/files).  This directive indicates if the device
   requires to be mounted using the {\bf Mount Command}.
   To be able to write a DVD, the following directives must also be
   defined: {\bf Mount Point},  {\bf Mount Command}, {\bf Unmount Command} and
   {\bf Write Part Command}.

\item [Mount Point = {\it directory}]
   \index[general]{Mount Point}
   Directory where the device can be mounted. 

\item [Mount Command = {\it name-string}]
   \index[general]{Mount Command}
   Command that must be executed to mount the device. Although the
   device is written directly, the mount command is necessary in
   order to determine the free space left on the DVD. Before the command is 
   executed, \%a is replaced with the Archive Device, and \%m with the Mount 
   Point.

   Most frequently, you will define it as follows:  

\footnotesize
\begin{verbatim}
  Mount Command = "/bin/mount -t iso9660 -o ro %a %m"
\end{verbatim}
\normalsize

However, if you have defined a mount point in /etc/fstab, you might be
able to use a mount command such as:

\footnotesize
\begin{verbatim}
  Mount Command = "/bin/mount /media/dvd"
\end{verbatim}
\normalsize


\item [Unmount Command = {\it name-string}]
   \index[general]{Unmount Command}
   Command that must be executed to unmount the device. Before the command  is
   executed, \%a is replaced with the Archive Device, and \%m with the  Mount
   Point.

   Most frequently, you will define it as follows:  

\footnotesize
\begin{verbatim}
  Unmount Command = "/bin/umount %m"
\end{verbatim}
\normalsize

\item [Write Part Command = {\it name-string}]
   \index[general]{Write Part Command  }
   Command that must be executed to write a part to the device. Before the 
   command is executed, \%a is replaced with the Archive Device, \%m with the 
   Mount Point, \%e is replaced with 1 if we are writing the first part,
   and with 0 otherwise, and \%v with the current part filename.

   For a DVD, you will most frequently specify the Bacula supplied  {\bf
   dvd-handler} script as follows:  

\footnotesize
\begin{verbatim}
  Write Part Command = "/path/dvd-handler %a write %e %v"
\end{verbatim}
\normalsize

  Where {\bf /path} is the path to your scripts install directory, and
  dvd-handler is the Bacula supplied script file.  
  This command will already be present, but commented out,
  in the default bacula-sd.conf file. To use it, simply remove
  the comment (\#) symbol.


\item [Free Space Command = {\it name-string}]
   \index[general]{Free Space Command  }
   Command that must be executed to check how much free space is left on the 
   device. Before the command is executed,\%a is replaced with the Archive
   Device.

   For a DVD, you will most frequently specify the Bacula supplied  {\bf
   dvd-handler} script as follows:  

\footnotesize
\begin{verbatim}
  Free Space Command = "/path/dvd-handler %a free"
\end{verbatim}
\normalsize

  Where {\bf /path} is the path to your scripts install directory, and
  dvd-handler is the Bacula supplied script file.
  If you want to specify your own command, please look at the code in
  dvd-handler to see what output Bacula expects from this command.
  This command will already be present, but commented out,
  in the default bacula-sd.conf file. To use it, simply remove
  the comment (\#) symbol.

  If you do not set it, Bacula will expect there is always free space on the
  device. 

\end{description}

In addition to the directives specified above, you must also
specify the other standard Device resource directives. Please see the
sample DVD Device resource in the default bacula-sd.conf file. Be sure
to specify the raw device name for {\bf Archive Device}. It should 
be a name such as {\bf /dev/cdrom} or {\bf /media/cdrecorder} or
{\bf /dev/dvd} depending on your system.  It will not be a name such
as {\bf /mnt/cdrom}.

Finally, for {\bf growisofs} to work, it must be able to lock
a certain amount of memory in RAM.  If you have restrictions on
this function, you may have failures.  Under {\bf bash}, you can
set this with the following command:

\footnotesize  
\begin{verbatim}
ulimit -l unlimited
\end{verbatim}
\normalsize

\section{Edit Codes for DVD Directives} 
\index[general]{Directives!DVD Edit Codes}
\index[general]{Edit Codes for DVD Directives }

Before submitting the {\bf Mount Command}, {\bf Unmount Command}, 
{\bf Write Part Command}, or {\bf Free Space Command} directives 
to the operating system, Bacula performs character substitution of the
following characters:

\footnotesize
\begin{verbatim}
    %% = %
    %a = Archive device name
    %e = erase (set if cannot mount and first part)
    %n = part number
    %m = mount point
    %v = last part name (i.e. filename)
\end{verbatim}
\normalsize



\section{DVD Specific Director Directives} 
\index[general]{Directives!DVD}
\index[general]{DVD Specific Director Directives }

The following directives are added to the Director's Job resource.
    
\label{WritePartAfterJob}
\begin{description}
\item [Write Part After Job = \lt{}yes|no\gt{}]
   \index[general]{Write Part After Job }
   If this directive is set to {\bf yes} (default {\bf no}), the
   Volume written to a temporary spool file for the current Job will
   be written to the DVD as a new part file
   will be created after the job is finished.  

   It should be set to {\bf yes} when writing to devices that require a mount
   (for example DVD), so you are sure that the current part, containing
   this job's data, is written to the device, and that no data is left in
   the temporary file on the hard disk.  However, on some media, like DVD+R
   and DVD-R, a lot of space (about 10Mb) is lost everytime a part is
   written.  So, if you run several jobs each after another, you could set
   this directive to {\bf no} for all jobs, except the last one, to avoid
   wasting too much space, but to ensure that the data is written to the
   medium when all jobs are finished.

   This directive is ignored for devices other than DVDs.
\end{description}



\label{DVDpoints}
\section{Other Points}
\index[general]{Points!Other }
\index[general]{Other Points }

\begin{itemize}
\item Please be sure that you have any automatic DVD mounting
   disabled before running Bacula -- this includes auto mounting
   in /etc/fstab, hotplug, ...  If the DVD is automatically
   mounted by the OS, it will cause problems when Bacula tries
   to mount/unmount the DVD.
\item Please be sure that you the directive {\bf Write Part After Job}
   set to {\bf yes}, otherwise the last part of the data to be
   written will be left in the DVD spool file and not written to
   the DVD. The DVD will then be unreadable until this last part
   is written.  If you have a series of jobs that are run one at
   a time, you can turn this off until the last job is run.
\item The current code is not designed to have multiple simultaneous
   jobs writing to the DVD.  As a consequence, please ensure that
   only one DVD backup job runs at any time.
\item Writing and reading of DVD+RW seems to work quite reliably
   provided you are using the patched dvd+rw-mediainfo programs.
   On the other hand, we do not have enough information to ensure
   that DVD-RW or other forms of DVDs work correctly.
\item DVD+RW supports only about 1000 overwrites. Every time you
   mount the filesystem read/write will count as one write. This can
   add up quickly, so it is best to mount your DVD+RW filesystem read-only.
   Bacula does not need the DVD to be mounted read-write, since it uses
   the raw device for writing.
\item Reformatting DVD+RW 10-20 times can apparently make the medium 
   unusable. Normally you should not have to format or reformat
   DVD+RW media. If it is necessary, current versions of growisofs will
   do so automatically.
\item We have had several problems writing to DVD-RWs (this does NOT
  concern DVD+RW), because these media have two writing-modes: {\bf
  Incremental Sequential} and {\bf Restricted Overwrite}.  Depending on
  your device and the media you use, one of these modes may not work
  correctly (e.g.  {\bf Incremental Sequential} does not work with my NEC
  DVD-writer and Verbatim DVD-RW).

  To retrieve the current mode of a DVD-RW, run:
\begin{verbatim}
  dvd+rw-mediainfo /dev/xxx
\end{verbatim}
  where you replace xxx with your DVD device name.

  {\bf Mounted Media} line should give you the information.

  To set the device to {\bf Restricted Overwrite} mode, run:
\begin{verbatim}
  dvd+rw-format /dev/xxx
\end{verbatim}
  If you want to set it back to the default {\bf Incremental Sequential} mode, run:
\begin{verbatim}
  dvd+rw-format -blank /dev/xxx
\end{verbatim}

\item Bacula only accepts to write to blank DVDs. To quickly blank a DVD+/-RW, run
  this command:
\begin{verbatim}
  dd if=/dev/zero bs=1024 count=512 | growisofs -Z /dev/xxx=/dev/fd/0
\end{verbatim}
  Then, try to mount the device, if it cannot be mounted, it will be considered
  as blank by Bacula, if it can be mounted, try a full blank (see below).

\item If you wish to blank completely a DVD+/-RW, use the following:
\begin{verbatim}
  growisofs -Z /dev/xxx=/dev/zero
\end{verbatim}
  where you replace xxx with your DVD device name. However, note that this
  blanks the whole DVD, which takes quite a long time (16 minutes on mine).
\item DVD+RW and DVD-RW support only about 1000 overwrites (i.e. don't use the
same medium for years if you don't want to have problems...).

To write to the DVD the first time use:
\begin{verbatim}
  growisofs -Z /dev/xxx filename
\end{verbatim}

To add additional files (more parts use):

\begin{verbatim}
  growisofs -M /dev/xxx filename
\end{verbatim}

The option {\bf -use-the-force-luke=4gms} was added in growisofs 5.20 to
override growisofs' behavior of always checking for the 4GB limit.
Normally, this option is recommended for all Linux 2.6.8 kernels or
greater, since these newer kernels can handle writing more than 4GB.
See below for more details on this subject.

\item For more information about DVD writing, please look at the
\elink{dvd+rw-tools homepage}{http://fy.chalmers.se/~appro/linux/DVD+RW/}.

\item According to bug \#912, bscan cannot read multi-volume DVDs.  This is
on our TODO list, but unless someone submits a patch it is not likely to be
done any time in the near future. (9 Sept 2007).

\end{itemize}
