%%
%%

\chapter{Dealing with Firewalls}
\label{FirewallsChapter}
\index[general]{Dealing with Firewalls }
\index[general]{Firewalls!Dealing with }

If you have a firewall or a DMZ installed on your computer, you may experience
difficulties contacting one or more of the Clients to back them up. This is
especially true if you are trying to backup a Client across the Internet. 

\section{Technical Details}
\index[general]{Technical Details }
\index[general]{Details!Technical }

If you are attempting to do this, the sequence of network events in Bacula to
do a backup are the following: 

\footnotesize
\begin{verbatim}
Console -> DIR:9101
DIR     -> SD:9103
DIR     -> FD:9102
FD      -> SD:9103
\end{verbatim}
\normalsize

Where hopefully it is obvious that DIR represents the Director, FD the File
daemon or client, and SD the Storage daemon. The numbers that follow those
names are the standard ports used by Bacula, and the \verb:->: represents the
left side making a connection to the right side (i.e. the right side is the
"server" or is listening on the specified port), and the left side is the
"client" that initiates the conversation. 

Note, port 9103 serves both the Director and the File daemon, each having its
own independent connection. 

If you are running {\bf iptables}, you might add something like: 

\footnotesize
\begin{verbatim}
-A FW-1-INPUT -m state --state NEW -m tcp -p tcp --dport 9101:9103 -j ACCEPT
\end{verbatim}
\normalsize

on your server, and 

\footnotesize
\begin{verbatim}
-A FW-1-INPUT -m state --state NEW -m tcp -p tcp --dport 9102 -j ACCEPT
\end{verbatim}
\normalsize

on your client. In both cases, I assume that the machine is allowed to
initiate connections on any port. If not, you will need to allow outgoing
connections on ports 9102 and 9103 on your server and 9103 on your client.
Thanks to Raymond Norton for this tip. 

\section{A Concrete Example}
\index[general]{Example!Concrete }
\index[general]{Concrete Example }

The following discussion was originally written by
Jesse Guardiani because he has 'internal' and 'external' requiring the
Director and the Client to use different IP addresses.  His original
solution was to define two different Storage resources in the Director's
conf file each pointing to the same Storage daemon but with different
IP addresses.  In Bacula 1.38.x this no longer works, because Bacula makes
a one-to-one association between a Storage daemon resource and a Device (such
as an Autochanger).  As a consequence, I have modified his original
text to a method that I believe will work, but is as of yet untested
(KES - July 2006).

My bacula server is on the 192.168.1.0/24 network at IP address 192.168.1.52.
For the sake of discussion we will refer to this network as the 'internal'
network because it connects to the internet through a NAT'd firewall. We will
call the network on the public (internet) side of the NAT'd firewall the
'external' network. Also, for the sake of discussion we will call my bacula
server: 

\footnotesize
\begin{verbatim}
    server.int.mydomain.tld
\end{verbatim}
\normalsize

when a fully qualified domain name is required, or simply: 

\footnotesize
\begin{verbatim}
    server
\end{verbatim}
\normalsize

if a hostname is adequate. We will call the various bacula daemons running on
the server.int.mydomain.tld machine: 

\footnotesize
\begin{verbatim}
    server-fd
    server-sd
    server-dir
\end{verbatim}
\normalsize

In addition, I have two clients that I want to back up with Bacula. The first
client is on the internal network. Its fully qualified domain name is: 

\footnotesize
\begin{verbatim}
    private1.int.mydomain.tld
\end{verbatim}
\normalsize

And its hostname is: 

\footnotesize
\begin{verbatim}
    private1
\end{verbatim}
\normalsize

This machine is a client and therefore runs just one bacula daemon: 

\footnotesize
\begin{verbatim}
    private1-fd
\end{verbatim}
\normalsize

The second client is on the external network. Its fully qualified domain name
is: 

\footnotesize
\begin{verbatim}
    public1.mydomain.tld
\end{verbatim}
\normalsize

And its hostname is: 

\footnotesize
\begin{verbatim}
    public1
\end{verbatim}
\normalsize

This machine also runs just one bacula daemon: 

\footnotesize
\begin{verbatim}
    public1-fd
\end{verbatim}
\normalsize

Finally, I have a NAT firewall/gateway with two network interfaces. The first
interface is on the internal network and serves as a gateway to the internet
for all the machines attached to the internal network (For example,
server.int.mydomain.tld and private1.int.mydomain.tld). The second interface
is on the external (internet) network. The external interface has been
assigned the name: 

\footnotesize
\begin{verbatim}
    firewall.mydomain.tld
\end{verbatim}
\normalsize

Remember: 

\footnotesize
\begin{verbatim}
    *.int.mydomain.tld = internal network
        *.mydomain.tld = external network
\end{verbatim}
\normalsize

\subsection{The Bacula Configuration Files for the Above}
\index[general]{Above!Bacula Configuration Files for the }
\index[general]{Bacula Configuration Files for the Above }

server-sd manages a 4 tape AIT autoloader. All of my backups are written to
server-sd. I have just *one* Device resource in my server-sd.conf file: 

\footnotesize
\begin{verbatim}
Autochanger {
  Name = "autochanger1";\
  Device = Drive0
  Changer Device = /dev/ch0;
  Changer Command = "/usr/local/sbin/chio-bacula %c %o %S %a";
}
Device {
  Name = Drive0
  DriveIndex = 0
  Media Type = AIT-1;
  Archive Device = /dev/nrsa1;
  Label Media = yes;
  AutoChanger = yes;
  AutomaticMount = yes;               # when device opened, read it
  AlwaysOpen = yes;
  Hardware End of Medium = No
  Fast Forward Space File = No
  BSF at EOM = yes
}
\end{verbatim}
\normalsize

(note, please see 
\ilink{the Tape Testing}{FreeBSDTapes} chapter of this manual
for important FreeBSD information.) However, unlike previously, there
is only one Storage definition in my server-dir.conf file: 

\footnotesize
\begin{verbatim}
Storage {
  Name = "autochanger1"    # Storage device for backing up
  Address = Storage-server
  SDPort = 9103
  Password = "mysecretpassword"
  Device = "autochanger1"
  Media Type = AIT-1
  Autochanger = yes
}
\end{verbatim}
\normalsize

Note that the Storage resource uses neither of the two addresses to
the Storage daemon -- neither server.int.mydomain.tld nor
firewall.mydomain.tld, but instead uses the address Storage-server.

What is key is that in the internal net, Storage-server is resolved
to server.int.mydomain.tld, either with an entry in /etc/hosts, or by
creating and appropriate DNS entry, and on the external net (the Client
machine), Storage-server is resolved to firewall.mydomain.tld.


In addition to the above, I have two Client resources defined in
server-dir.conf: 

\footnotesize
\begin{verbatim}
Client {
  Name = private1-fd
  Address = private1.int.mydomain.tld
  FDPort = 9102
  Catalog = MyCatalog
  Password = "mysecretpassword"       # password for FileDaemon
}
Client {
  Name = public1-fd
  Address = public1.mydomain.tld
  FDPort = 9102
  Catalog = MyCatalog
  Password = "mysecretpassword"       # password for FileDaemon
}
\end{verbatim}
\normalsize

And finally, to tie it all together, I have two Job resources defined in
server-dir.conf: 

\footnotesize
\begin{verbatim}
Job {
  Name = "Private1-Backup"
  Type = Backup
  Client = private1-fd
  FileSet = "Private1"
  Schedule = "WeeklyCycle"
  Storage = "autochanger1-int"
  Messages = Standard
  Pool = "Weekly"
  Write Bootstrap = "/var/db/bacula/Private1-Backup.bsr"
  Priority = 12
}
Job {
  Name = "Public1-Backup"
  Type = Backup
  Client = public1-fd
  FileSet = "Public1"
  Schedule = "WeeklyCycle"
  Storage = "autochanger1-ext"
  Messages = Standard
  Pool = "Weekly"
  Write Bootstrap = "/var/db/bacula/Public1-Backup.bsr"
  Priority = 13
}
\end{verbatim}
\normalsize

It is important to notice that because the 'Private1-Backup' Job is intended
to back up a machine on the internal network so it resolves Storage-server
to contact the Storage daemon via the internal net.
On the other hand, the 'Public1-Backup' Job is intended to
back up a machine on the external network, so it resolves Storage-server
to contact the Storage daemon via the external net.

I have left the Pool, Catalog, Messages, FileSet, Schedule, and Director
resources out of the above server-dir.conf examples because they are not
pertinent to the discussion. 

\subsection{How Does It Work?}
\index[general]{How Does It Work? }
\index[general]{Work!How Does It }

If I want to run a backup of private1.int.mydomain.tld and store that backup
using server-sd then my understanding of the order of events is this: 

\begin{enumerate}
\item I execute my Bacula 'console' command on server.int.mydomain.tld.  
\item console connects to server-dir.  
\item I tell console to 'run' backup Job 'Private1-Backup'.  
\item console relays this command to server-dir.  
\item server-dir connects to private1-fd at private1.int.mydomain.tld:9102  
\item server-dir tells private1-fd to start sending the files defined in  the
   'Private1-Backup' Job's FileSet resource to the Storage resource 
   'autochanger1', which we have defined in server-dir.conf as having the 
address:port of Storage-server, which is mapped by DNS to server.int.mydomain.tld.
\item private1-fd connects to server.int.mydomain.tld:9103 and begins sending 
   files. 
   \end{enumerate}

Alternatively, if I want to run a backup of public1.mydomain.tld and store
that backup using server-sd then my understanding of the order of events is
this: 

\begin{enumerate}
\item I execute my Bacula 'console' command on server.int.mydomain.tld.  
\item console connects to server-dir.  
\item I tell console to 'run' backup Job 'Public1-Backup'.  
\item console relays this command to server-dir.  
\item server-dir connects, through the NAT'd firewall, to public1-fd at 
   public1.mydomain.tld:9102  
\item server-dir tells public1-fd to start sending the files defined in  the
   'Public1-Backup' Job's FileSet resource to the Storage resource 
   'autochanger1', which we have defined in server-dir.conf as having the 
   same address:port as above of Storage-server, but which on this machine
   is resolved to firewall.mydomain.tld:9103.  
\item public1-fd connects to firewall.mydomain.tld:9103 and begins sending 
   files. 
   \end{enumerate}

\subsection{Important Note}
\index[general]{Important Note }
\index[general]{Note!Important }

In order for the above 'Public1-Backup' Job to succeed,
firewall.mydomain.tld:9103 MUST be forwarded using the firewall's
configuration software to server.int.mydomain.tld:9103. Some firewalls call
this 'Server Publication'. Others may call it 'Port Forwarding'. 

\subsection{Firewall Problems}
\index[general]{Firewall Problems}
\index[general]{Problems!Firewalls}
Either a firewall or a router may decide to timeout and terminate
open connections if they are not active for a short time. By Internet
standards the period should be two hours, and should be indefinitely
extended if KEEPALIVE is set as is the case by Bacula.  If your firewall
or router does not respect these rules, you may find Bacula connections
terminated. In that case, the first thing to try is turning on the
{\bf Heart Beat Interval} both in the File daemon and the Storage daemon
and set an interval of say five minutes.

Also, if you have denial of service rate limiting in your firewall, this
too can cause Bacula disconnects since Bacula can at times use very high 
access rates. To avoid this, you should implement default accept
rules for the Bacula ports involved before the rate limiting rules.

Finally, if you have a Windows machine, it will most likely by default
disallow connections to the Bacula Windows File daemon.  See the      
Windows chapter of this manual for additional details.
