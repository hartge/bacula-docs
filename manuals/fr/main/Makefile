#
#  Makefile for Bacula LaTeX  Manual
#
# To build everything do
#    make tex
#    make web
#    make html
#    make dvipdf
#
# or simply
#
#    make
#
# for rapid development do:
#    make tex
#    make show
#
#
# If you are having problems getting "make" to work, debugging it is
#  easier if can see the output from latex, which is normally redirected
#  to /dev/null.  To see it, do the following:
#
#   cd docs/manual
#   make tex
#   latex bacula.tex
#
#  typically the latex command will stop indicating the error (e.g. a
#  missing \ in front of a _ or a missing { or ] ...
#
#  The following characters must be preceded by a backslash
#    to be entered as printable characters:
# 
#    # $ % & ~ _ ^ \ { }
#

IMAGES=../../../images

DOC=main
MAINDOC=Bacula_Main_Reference.html

first_rule: all

all: tex web dvipdf mini-clean

.SUFFIXES:     .tex .html
.PHONY:
.DONTCARE:


tex:
	@../../update_version
	@echo "Making version `cat version.tex`"
	@cp -fp ${IMAGES}/hires/*.eps .
	@touch ${DOC}i-dir.tex ${DOC}i-fd.tex ${DOC}i-sd.tex \
	     ${DOC}i-console.tex ${DOC}i-general.tex
	latex -interaction=batchmode ${DOC}.tex
	makeindex ${DOC}.idx -o ${DOC}.ind 2>/dev/null
	makeindex ${DOC}.ddx -o ${DOC}.dnd >/dev/null 2>/dev/null
	makeindex ${DOC}.fdx -o ${DOC}.fnd >/dev/null 2>/dev/null
	makeindex ${DOC}.sdx -o ${DOC}.snd >/dev/null 2>/dev/null
	makeindex ${DOC}.cdx -o ${DOC}.cnd >/dev/null 2>/dev/null
	latex -interaction=batchmode ${DOC}.tex

pdf:
	@echo "Making pdfm"
	@cp -fp ${IMAGES}/hires/*.eps .
	dvipdfm -p a4 ${DOC}.dvi

dvipdf:
	@echo "Making dvi to pdf"
	@cp -fp ${IMAGES}/hires/*.eps .
	dvipdf ${DOC}.dvi ${DOC}.pdf

html:
	@echo " "
	@echo "Making html"
	@cp -fp ${IMAGES}/*.eps .
	@rm -f next.eps next.png prev.eps prev.png up.eps up.png
	@(if [ -f imagename_translations ] ; then \
	    ./translate_images.pl --from_meaningful_names ${DOC}.html; \
	 fi)
	latex2html -white -no_subdir -split 0 -toc_stars -white -notransparent \
		-init_file latex2html-init.pl ${DOC} >tex.out 2>&1
	@(if [ -f imagename_translations ] ; then \
	    ./translate_images.pl --from_meaningful_names ${DOC}.html; \
	 fi)
	(cd ${DOC}; for i in *.png ; do cp -fp ../${IMAGES}/$${i} . 2>/dev/null; done)
	@echo "Done making html"

web:
	@echo "Making web"
	@rm -rf ${DOC}
	@mkdir -p ${DOC}
	@cp -fp ${IMAGES}/*.eps .
	@rm -f next.eps next.png prev.eps prev.png up.eps up.png
	latex2html -split 3 -local_icons -t "Bacula Main Reference" -long_titles 4 \
		-toc_stars -contents_in_nav -init_file latex2html-init.pl \
		-no_antialias -no_antialias_text \
		-white -notransparent ${DOC} >tex.out 2>&1
	@(if [ -f imagename_translations ] ; then \
	    ./translate_images.pl --from_meaningful_names ${DOC}.html; \
	 fi)
	@cp -f ${DOC}/${MAINDOC} ${DOC}/index.html
	@echo "Done making web"
show:
	xdvi ${DOC}

texcheck:
	./check_tex.pl ${DOC}.tex

main_configs:
	pic2graph -density 100 <main_configs.pic >main_configs.png

mini-clean:
	@rm -f 1 2 3 *.tex~
	@rm -f *.gif *.jpg *.eps
	@rm -f *.aux *.cp *.fn *.ky *.log *.pg
	@rm -f *.backup *.ilg *.lof *.lot
	@rm -f *.cdx *.cnd *.ddx *.ddn *.fdx *.fnd *.ind *.sdx *.snd
	@rm -f *.dnd *.old *.out 
	@rm -f ${DOC}/*.gif ${DOC}/*.jpg ${DOC}/*.eps
	@rm -f ${DOC}/*.aux ${DOC}/*.cp ${DOC}/*.fn ${DOC}/*.ky ${DOC}/*.log ${DOC}/*.pg
	@rm -f ${DOC}/*.backup ${DOC}/*.ilg ${DOC}/*.lof ${DOC}/*.lot
	@rm -f ${DOC}/*.cdx ${DOC}/*.cnd ${DOC}/*.ddx ${DOC}/*.ddn ${DOC}/*.fdx ${DOC}/*.fnd ${DOC}/*.ind ${DOC}/*.sdx ${DOC}/*.snd
	@rm -f ${DOC}/*.dnd ${DOC}/*.old ${DOC}/*.out
	@rm -f ${DOC}/WARNINGS


clean:
	@rm -f 1 2 3 *.tex~
	@rm -f *.png *.gif *.jpg *.eps
	@rm -f *.pdf *.aux *.cp *.fn *.ky *.log *.pg
	@rm -f *.html *.backup *.ps *.dvi *.ilg *.lof *.lot
	@rm -f *.cdx *.cnd *.ddx *.ddn *.fdx *.fnd *.ind *.sdx *.snd
	@rm -f *.dnd imagename_translations
	@rm -f *.old WARNINGS *.out *.toc *.idx
	@rm -f ${DOC}i-*.tex
	@rm -rf ${DOC}


distclean: clean
	@rm -f images.pl labels.pl internals.pl
